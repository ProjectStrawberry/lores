package com.gmail.jeremypiemonte.lores;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.ConditionFailedException;
import co.aikar.commands.PaperCommandManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bukkit.ChatColor.COLOR_CHAR;

public class Main extends JavaPlugin {
    public static Main plugin;
    private static final Set<ChatColor> COLORS = EnumSet.of(ChatColor.BLACK, ChatColor.DARK_BLUE, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA, ChatColor.DARK_RED, ChatColor.DARK_PURPLE, ChatColor.GOLD, ChatColor.GRAY, ChatColor.DARK_GRAY, ChatColor.BLUE, ChatColor.GREEN, ChatColor.AQUA, ChatColor.RED, ChatColor.LIGHT_PURPLE, ChatColor.YELLOW, ChatColor.WHITE);
    private static final Set<ChatColor> FORMATS = EnumSet.of(ChatColor.BOLD, ChatColor.STRIKETHROUGH, ChatColor.UNDERLINE, ChatColor.ITALIC, ChatColor.RESET);
    private static final Set<ChatColor> MAGIC = EnumSet.of(ChatColor.MAGIC);
    private static final Pattern STRIP_RGB_PATTERN = Pattern.compile("\u00a7x((?:\u00a7[0-9a-fA-F]){6})");
    private static final Pattern STRIP_ALL_PATTERN = Pattern.compile("\u00a7+([0-9a-fk-orA-FK-OR])");

    @Override
    public void onEnable() {
        plugin = this;

        Field f;
        try {
            f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
            Enchantment.registerEnchantment(new GlowEnchant());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        PaperCommandManager commandManager = new PaperCommandManager(this);
        commandManager.enableUnstableAPI("help");
        commandManager.registerCommand(new Commands(this));

        commandManager.getCommandConditions().addCondition("itemInHand", (context) -> {
            BukkitCommandIssuer issuer = context.getIssuer();
            if (!issuer.isPlayer() || issuer.getPlayer().getInventory().getItemInMainHand().getType() == Material.AIR) {
                throw new ConditionFailedException("You need an item in your hand to use this command.");
            }
        });

        commandManager.getCommandCompletions().registerAsyncCompletion("itemName", c -> {
            List<String> completion = new ArrayList<>();
            ItemStack item = c.getPlayer().getInventory().getItemInMainHand();
            if (item.getType() == Material.AIR) return completion;

            ItemMeta meta = item.getItemMeta();
            assert meta != null;

            if (meta.hasDisplayName()) completion.add(unformatString(meta.getDisplayName()));

            return completion;
        });

        commandManager.getCommandCompletions().registerCompletion("ItemFlags", c -> {
            List<String> itemFlags = new ArrayList<>();

            for (ItemFlag flag : ItemFlag.values()) {
                itemFlags.add(flag.toString());
            }

           return itemFlags;
        });

        commandManager.getCommandCompletions().registerCompletion("line", c -> {
            List<String> completion = new ArrayList<>();
            ItemStack item = c.getPlayer().getInventory().getItemInMainHand();
            if (item.getType() == Material.AIR) return completion;

            ItemMeta meta = item.getItemMeta();
            assert meta != null;
            if (!meta.hasLore()) return completion;
            List<String> lore = meta.getLore();
            assert lore != null;

            for (int i=1; i<=lore.size(); i++) completion.add(String.valueOf(i));

            return completion;
        });

        commandManager.getCommandCompletions().registerCompletion("lore", c -> {
            List<String> completion = new ArrayList<>();
            ItemStack item = c.getPlayer().getInventory().getItemInMainHand();
            if (item.getType() == Material.AIR) return completion;

            ItemMeta meta = item.getItemMeta();
            assert meta != null;
            if (!meta.hasLore()) return completion;
            List<String> lore = meta.getLore();
            assert lore != null;

            int lineNumber = c.getContextValueByName(Integer.class, "lineNumber");
            if (lineNumber > 0 && lore.size() >= (lineNumber - 1)) completion.add(unformatString(lore.get(lineNumber - 1)));

            return completion;
        });

        getLogger().info("Lore loaded.");
    }

    private EnumSet<ChatColor> getSupported() {
        EnumSet<ChatColor> supported = EnumSet.noneOf(ChatColor.class);
        supported.addAll(COLORS);
        supported.addAll(FORMATS);
        supported.addAll(MAGIC);

        return supported;
    }

    public String unformatString(String message) {
        if (message == null) {
            return null;
        }
        EnumSet<ChatColor> supported = getSupported();

        // RGB Codes
        StringBuffer rgbBuilder = new StringBuffer();
        Matcher rgbMatcher = STRIP_RGB_PATTERN.matcher(message);
        while (rgbMatcher.find()) {
            String code = rgbMatcher.group(1).replace("\u00a7", "");
            rgbMatcher.appendReplacement(rgbBuilder, "&#" + code);
        }
        rgbMatcher.appendTail(rgbBuilder);
        message = rgbBuilder.toString(); // arreter de parler

        // Legacy Colors
        StringBuffer builder = new StringBuffer();
        Matcher matcher = STRIP_ALL_PATTERN.matcher(message);
        searchLoop: while (matcher.find()) {
            char code = matcher.group(1).toLowerCase(Locale.ROOT).charAt(0);
            for (ChatColor color : supported) {
                if (color.getChar() == code) {
                    matcher.appendReplacement(builder, "&" + code);
                    continue searchLoop;
                }
            }
            matcher.appendReplacement(builder, "");
        }
        matcher.appendTail(builder);
        return builder.toString();
    }

    public String parseColorCodes(String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);
        str = translateHexColorCodes("#", "", str);

        return str;
    }

    public String translateHexColorCodes(String startTag, String endTag, String message) {
        final Pattern hexPattern = Pattern.compile(startTag + "([A-Fa-f0-9]{6})" + endTag);
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    }
}

