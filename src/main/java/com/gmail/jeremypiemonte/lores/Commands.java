package com.gmail.jeremypiemonte.lores;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@CommandAlias("lore|lores")
public class Commands extends BaseCommand {

    Main plugin;

    Commands(Main p) {
        plugin = p;
    }

    @Subcommand("name|title")
    @Description("Sets the display name on an item")
    @CommandPermission("lores.modify")
    @CommandCompletion("@itemName")
    @Conditions("itemInHand")
    public void name(Player player, String name) {
        ItemStack item = player.getInventory().getItemInMainHand();

        name = plugin.parseColorCodes(name);

        ItemMeta meta = item.getItemMeta();
        assert meta != null;
        meta.setDisplayName(name);
        meta = editedBy(player.getUniqueId().toString(), meta);
        item.setItemMeta(meta);
        player.sendMessage(ChatColor.DARK_GREEN + "You've set the display name on this item to " + ChatColor.RESET + name);
    }

    @Subcommand("unname")
    @Description("Removes the name from an item")
    @CommandPermission("lores.unname")
    @Conditions("itemInHand")
    public void unname(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        meta.setDisplayName(null);
        item.setItemMeta(meta);
        player.sendMessage(ChatColor.DARK_GREEN + "The display name on your item has been cleared.");
    }

    @Subcommand("add")
    @Description("Adds a line to an items lore")
    @CommandPermission("lores.modify")
    @CommandCompletion("<lore>")
    @Conditions("itemInHand")
    public void add(Player player, String line) {
        ItemStack item = player.getInventory().getItemInMainHand();

        line = plugin.parseColorCodes(line);

        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        List<String> lore = (meta.hasLore() ? meta.getLore() : new ArrayList<>());
        assert lore != null;
        lore.add(line);

        meta.setLore(lore);
        meta = editedBy(player.getUniqueId().toString(), meta);
        item.setItemMeta(meta);

        player.sendMessage(ChatColor.DARK_GREEN + "You've added a line to this items lore: " + ChatColor.RESET + line);
    }

    @Subcommand("edit")
    @Description("Edits an items lore")
    @CommandPermission("lores.modify")
    @CommandCompletion("@line @lore")
    @Conditions("itemInHand")
    public void edit(Player player, int lineNumber, String line) {
        ItemStack item = player.getInventory().getItemInMainHand();

        if (lineNumber < 0) lineNumber = 0;
        if (lineNumber > 0) lineNumber--;

        line = plugin.parseColorCodes(line);
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        List<String> lore = (meta.hasLore() ? meta.getLore() : new ArrayList<>());
        assert lore != null;

        while (lore.size() < (lineNumber + 1)) lore.add("");

        lore.set(lineNumber, line);
        meta.setLore(lore);
        meta = editedBy(player.getUniqueId().toString(), meta);
        item.setItemMeta(meta);

        player.sendMessage(ChatColor.DARK_GREEN + "You've set the lore on that line to: " + ChatColor.RESET + line);
    }

    @Subcommand("removeline")
    @Description("Removes a line from an items lore")
    @CommandPermission("lores.modify")
    @CommandCompletion("@line")
    @Conditions("itemInHand")
    public void removeline(Player player, int lineNumber) {
        ItemStack item = player.getInventory().getItemInMainHand();

        if (lineNumber < 0) lineNumber = 0;
        if (lineNumber > 0) lineNumber--;

        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        List<String> lore = (meta.hasLore() ? meta.getLore() : new ArrayList<>());
        assert lore != null;

        if (lore.size() < (lineNumber + 1)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That line does not exist.");
            return;
        }

        lore.remove(lineNumber);
        meta.setLore(lore);
        item.setItemMeta(meta);

        player.sendMessage(ChatColor.DARK_GREEN + "You've removed that line from this items lore.");
    }

    @Subcommand("clear|reset")
    @Description("Clears an items lore")
    @CommandPermission("lores.modify")
    @Conditions("itemInHand")
    public void clear(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        meta.setLore(new ArrayList<>());
        meta = editedBy(player.getUniqueId().toString(), meta);
        item.setItemMeta(meta);

        player.sendMessage(ChatColor.DARK_GREEN + "You've reset the lore on this item.");
    }

    @Subcommand("glow|shine|shiny")
    @Description("Makes an item glow")
    @CommandPermission("lores.modify")
    @Conditions("itemInHand")
    public void glow(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();

        Enchantment glowEnchant = Enchantment.getByKey(new NamespacedKey(Main.plugin, "glow"));
        assert glowEnchant != null;

        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        if (meta.hasEnchant(glowEnchant)) {
            meta.removeEnchant(glowEnchant);
            meta = editedBy(player.getUniqueId().toString(), meta);
            item.setItemMeta(meta);

            player.sendMessage(ChatColor.DARK_GREEN + "Your item no longer glows.");
        } else {
            meta.addEnchant(glowEnchant, 1, false);
            meta = editedBy(player.getUniqueId().toString(), meta);
            item.setItemMeta(meta);

            player.sendMessage(ChatColor.DARK_GREEN + "Your item now glows.");
        }
    }

    @Subcommand("flag")
    @Description("Toggle item flags")
    @CommandPermission("lores.modify")
    @CommandCompletion("@ItemFlags")
    @Conditions("itemInHand")
    public void flag(Player player, ItemFlag flag) {
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        if (meta.hasItemFlag(flag)) {
            meta.removeItemFlags(flag);
            player.sendMessage(ChatColor.DARK_GREEN + "You've removed the " + ChatColor.GREEN + flag.toString() + ChatColor.DARK_GREEN + " flag from this item.");
        } else {
            meta.addItemFlags(flag);
            player.sendMessage(ChatColor.DARK_GREEN + "You've added the " + ChatColor.GREEN + flag.toString() + ChatColor.DARK_GREEN + " flag to this item.");
        }

        meta = editedBy(player.getUniqueId().toString(), meta);
        item.setItemMeta(meta);
    }

    @Subcommand("info")
    @Description("Displays information about a lored item")
    @CommandPermission("lores.modify")
    @Conditions("itemInHand")
    public void info(Player player) {
        ItemStack item = player.getInventory().getItemInMainHand();
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        PersistentDataContainer container = meta.getPersistentDataContainer();
        if (!container.has(new NamespacedKey(plugin, "editedBy"), PersistentDataType.STRING) || !container.has(new NamespacedKey(plugin, "editDate"), PersistentDataType.LONG)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This item doesn't have any information stored.");
            return;
        }

        OfflinePlayer targetPlayer = Bukkit.getOfflinePlayer(UUID.fromString(Objects.requireNonNull(container.get(new NamespacedKey(plugin, "editedBy"), PersistentDataType.STRING))));
        long date = container.get(new NamespacedKey(plugin, "editDate"), PersistentDataType.LONG);

        player.sendMessage(ChatColor.DARK_GREEN + "Item last edited by " + ChatColor.GREEN + targetPlayer.getName() + ChatColor.DARK_GREEN + " on " + ChatColor.GREEN + new Timestamp(date).toString());
    }

    @Subcommand("test")
    @Description("Preview a lore")
    public void test(CommandSender player, String line) {
        player.sendMessage(plugin.parseColorCodes(line));
    }

    @Default
    @HelpCommand
    public void doHelp(CommandHelp help) {
        help.showHelp();
    }

    public ItemMeta editedBy(String uuid, ItemMeta meta) {
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "editedBy"), PersistentDataType.STRING, uuid);
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, "editDate"), PersistentDataType.LONG, System.currentTimeMillis());

        return meta;
    }
}
